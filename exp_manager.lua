-- EXP is disabled right now as we haven't decided yet what purpose should have,
-- aside as a barrier for future ranked games

function quake.add_xp(p_name, xp)
  quake.players[p_name].XP = quake.players[p_name].XP + xp
end

function quake.subtract_exp(p_name, xp)
  quake.players[p_name].XP = quake.players[p_name].XP - xp
  if quake.players[p_name].XP < 0 then
    quake.players[p_name].XP = 0
  end
end
