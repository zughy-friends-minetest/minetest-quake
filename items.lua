minetest.register_tool("quake:match_over", {

  description = S("THE END!"),
  inventory_image = "quake_match_over.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "2"},

})
