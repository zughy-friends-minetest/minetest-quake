--WIP AF
quake.register_weapon("quake:shotgun", {
  description = "Keep your friends close, and your enemies closer",
  mesh = "quake_railgun.obj",
  tiles = {"quake_railgun.png"},
  wield_scale = {x=1.3, y=1.3, z=1.3},
  inventory_image = "quake_shotgun_icon.png",

  weap_damage = 10,
  weap_delay = 0.8,
  weap_sound_shooting = "quake_shotgun_shoot",
  is_hitscan = true,
  range = 8,
  has_knockback = true
})
