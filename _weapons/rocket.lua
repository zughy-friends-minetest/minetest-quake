quake.register_bullet("quake:rocket",{
  physical = true,
  collide_with_objects = true,
  mesh = "quake_rocket.obj",
  visual = "mesh",
  visual_size = {x=1, y=1, z=1},
  textures = {"quake_bullet_rocket.png"},
  explosion_texture = "quake_rocket_particle.png",
  collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},  -- {xmin, ymin, zmin, xmax, ymax, zmax}
  speed = 30,
  damage = 14,
  jump = 0,
  explosion_range = 4,
  duration = 5,
  explosion_damage = 16,
  on_destroy = quake.explode,

})
