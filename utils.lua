function quake.calc_kill_leader(arena, killer)

  if arena.kill_leader == "" then
    arena.kill_leader = killer
  return end

  if arena.players[killer].kills > arena.players[arena.kill_leader].kills then
    arena.kill_leader = killer
  end

end
